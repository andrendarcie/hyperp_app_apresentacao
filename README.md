# README #

### Sobre o projeto ###

* HyperP app Apresentação faz parte do Hyper P, plataforma de apresentação de áudio, vídeos, textos e imagens com
  interação do apresentador e público.
+ HyperP app Apresentação é divido em duas partes:
    * O **servidor** é responsável por gerenciar as conexões dos dispositivos clientes, tanto apresentadores quanto espectadores.
    * A **apresentação** é responsável por exibir o conteúdo  de audio, vídeo e imagem.
- versão: 0.1

### Como faço para configurar? ###

* Esse projeto depende do Java instalado no computador juntamente com os arquivos
de execução que podem ser encontrados em: [download](https://drive.google.com/drive/folders/0B3oPhzMLgVFZbVg3eUpyNnh5VnM?usp=sharing)
* Após do download dos arquivos necessários coloque a pasta principal em *C:\SMH*
+ Dependências:
    * Java SE versão 8
    * Java FX versão 8
    * IDE: NetBeans versão 8.1
